'use strict';

/* Ответы на теоритические вопросы:
1. document.createElement(tag) - это первый метод, или методом кронирования cloneNode(true or false (с детьми или без)) 
2. Первый параметр функции .insertAdjacentHTML() это position - оценка элемента относительно элемента вызвавшего метод
принимает значения beforebegin, afterbegin, beforeend, afterend
3. Удалить элемент можно с помощью метода .remove или .removeChild для дочернего элемента
*/


let domArray = ["Dimon", "Stas", "Maks", "Oksana", "Denis", 228];

const createList = (arr, place = document.body) => {
	const ul = document.createElement('ul');
	place.append(ul);

	arr.forEach(element => {
		let li = document.createElement('li');
		if (!Array.isArray(element)) {
			li.innerHTML = element;
		} else {
			createList (element, li);
		} 
		ul.appendChild(li);
	});
	console.log(ul);
};

let arr = ['Privet', 'Andrey', 'Ny', 'Gde', 'Tu', 'Bul', ['ya', 'bul', 'tyt']];

createList(arr);
createList(domArray);


